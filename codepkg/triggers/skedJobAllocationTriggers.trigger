trigger skedJobAllocationTriggers on sked__Job_Allocation__c (after insert, after update, after delete) {
    skedTriggerUtil.isFromJobAllocation = true;

    if(Trigger.isInsert){
        //Create new Events for Dispatched Job Allocations
        list<Id> jaIDs = skedTriggerUtil.getNewDispatchedJobAllocationIDs( Trigger.new );
        skedTriggerUtil.syncJobAllocationsToEvents( jaIDs );
    }

    if(Trigger.isUpdate){
        //Create new Events for Dispatched Job Allocations
        list<Id> jaIDs = skedTriggerUtil.getJobAllocationIDs_StatusChanged( new Set<String>{'Dispatched'}, Trigger.new, Trigger.old, Trigger.oldMap );
        skedTriggerUtil.syncJobAllocationsToEvents( jaIDs );

        //Delete Events when Job are Declined
        list<Id> deletedJAIDs = skedTriggerUtil.getJobAllocationIDs_StatusChanged( new Set<String>{'Declined','Deleted','Complete'}, Trigger.new, Trigger.old, Trigger.oldMap );
        list<Event> lstEvent = [select id from Event where Job_Allocation_ID__c in:deletedJAIDs];
        if(lstEvent.size() > 0){
            delete lstEvent;
        }
    }
    //Delete Job Allocation Delete Events
    if(Trigger.isDelete){
        skedTriggerUtil.deleteRelatedEvents( Trigger.old );
    }

}