trigger skedJobTriggers on sked__Job__c (after insert ,after update, before Delete) {
    skedTriggerUtil.isFromJob = true;
    set<string> ignoreStatuses = new set<string>{'Pending Dispatch','Complete','Declined','Deleted'};

    if(trigger.isAfter){
        if(trigger.isInsert){
            //skedTriggerUtil.createAgreeServicer(trigger.new);            
        }

        if(Trigger.isUpdate){
            //Update related events, ASP events if job start/end time is changed
            list<Id> jobIDs = skedTriggerUtil.getJobIDs_StartEndTimeChanged(Trigger.new, Trigger.old, Trigger.oldMap );
            map<id, sked__Job_Allocation__c> relatedJAMap = new map<id,sked__Job_Allocation__c>([select Id from sked__Job_Allocation__c where sked__Job__c in:jobIDs and sked__Status__c not in :ignoreStatuses ]);
            skedTriggerUtil.syncJobAllocationsToEvents( new list<Id>( relatedJAMap.keySet() ) );
            //Delete related events if job's status is cancelled
            List<Id> cancelledJobIDs = skedTriggerUtil.getJobIDs_StatusChanged(new Set<String>{ 'Cancelled' }, Trigger.new, Trigger.old, Trigger.oldMap );
            list<sked__Job_Allocation__c> jaList = [select Id from sked__Job_Allocation__c where sked__Job__c in:cancelledJobIDs];
            skedTriggerUtil.deleteRelatedEvents( jaList );                   
        }

    }

    if(trigger.isBefore){
        if(Trigger.isDelete){
            //Explicitly delete related Job Allocations so that related Events are deleted as well.
            list<sked__Job_Allocation__c> jaList = [select id from sked__Job_Allocation__c where sked__Job__c in :trigger.old];
            skedTriggerUtil.deleteRelatedEvents( jaList );
        }
    }
}