trigger skedEventTrigger on Event (after insert, after update, after delete, before delete) {
	skedTriggerUtil.isFromEvent = true;

	if( Trigger.isAfter){
        if(Trigger.isUpdate){
			for(Event e: trigger.new){
				if(!String.isBlank(e.Job_Allocation_ID__c) && !skedTriggerUtil.isFromJob && !skedTriggerUtil.isFromJobAllocation){
					e.addError('This event is Locked');
				}
			}

		}

		if(Trigger.isDelete){
			for(Event e: trigger.old){
				if(!String.isBlank(e.Job_Allocation_ID__c) && !skedTriggerUtil.isFromJob && !skedTriggerUtil.isFromJobAllocation){
					e.addError('This event is Locked');
				}
			}
			//Delete related Activities
			skedTriggerUtil.deleteActivities( Trigger.old );
		}

		if(Trigger.isInsert || Trigger.isUpdate){
            if(skedTriggerUtil.isFromJobAllocation)  return;
            skedTriggerUtil.syncEventsToActivities( Trigger.new, Trigger.isUpdate);
		}
	}
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            //When an Event Series is deleted the main event deleted and delete trigger event is fired, however, 
			//other events are deleted without firing delete trigger event. So we have to explicitly delete these event 
			//so that corresponding activities are deleted
			skedTriggerUtil.deleteEventSeries(Trigger.old);
        }
    }
	
}