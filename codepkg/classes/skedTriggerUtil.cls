public  class skedTriggerUtil {
    public static boolean isFromJobAllocation = false;
    public static boolean isFromSkeduloActivity = false;
    public static boolean isFromEvent = false;
    public static boolean isFromJob = false;

    /*
    * Sync Activities to Event when Activities are upserted
    */
    public static void syncActivitiesToEvents(list<sked__Activity__c> activityList){
        if( activityList==null || activityList.isEMpty() ) return;
        //Get source activities (i.e Activities that are not referencing to other events)
        list<sked__Activity__c> sourceList = new list<sked__Activity__c>();
        for(sked__Activity__c a : activityList){
            if( String.isBlank( a.Event_ID__c) ) sourceList.add( a );
        }
        if(sourceList.isEmpty()) return;

        list<Event> eventsToUpsert = new list<Event>();
        list<ID> resourceIDs    = new list<ID> ();
        list<String> activityIDs    = new list<String> ();

        for(sked__Activity__c act: sourceList){
            if(act.sked__Resource__c != null){
                resourceIDs.add(act.sked__Resource__c);
                activityIDs.add( act.Id );
            }
        }
        //Map Resource ID to Resource to get User's Id 
        map<id,sked__Resource__c> map_Id_Res = new map<id,sked__Resource__c>([select id , sked__User__c from sked__Resource__c where id in:resourceIDs and sked__User__c != null]);
        //Search for related Events
        list<Event> relatedEvents = [Select Id, Activity_ID__c from Event where Activity_ID__c IN :activityIDs];
        Map<Id,Id> mapActivityIdToEventId = new Map<Id,Id>();
        for( Event e : relatedEvents ){
            mapActivityIdToEventId.put( Id.valueOf(e.Activity_ID__c), e.Id );
        }

        for(sked__Activity__c act: sourceList){
            if(act.sked__Resource__c != null && map_Id_Res.containskey(act.sked__Resource__c)){
                Id eventId = mapActivityIdToEventId.containsKey( act.Id )? mapActivityIdToEventId.get( act.Id ) : null;
                eventsToUpsert.add( 
                    syncActivityToEvent(act, map_Id_Res.get(act.sked__Resource__c).sked__User__c, eventId)
                );
            }
        }

        if(eventsToUpsert.size() > 0){
            upsert eventsToUpsert;
        }   
    }

    /*
    * Sync an Activity to an Event
    */
    public static Event syncActivityToEvent(sked__Activity__c activity, Id userId, Id eventId){
        Event event = new Event(
            Subject         = activity.sked__Type__C,
            OwnerId         = userId,
            Location        = activity.sked__Address__c,
            StartDateTime   = activity.sked__Start__c,
            EndDateTime     = activity.sked__End__c,
            Description     = activity.sked__Notes__c,
            Activity_ID__c  = activity.Id
        );
        if(eventId != null){//Update an existing event
            event.Id    = eventId;
        }
        return event;
    }

    /*
    * Sync Events to Activities when Events are upserted
    */
    /*
    @future
    public static void syncEventsToActivitiesAsync(Set<Id> eventIDs){ 
        list<Event> eventList = [Select id, Activity_ID__c, OwnerId, Job_Allocation_ID__c, Subject, Description,
                                 StartDateTime, EndDateTime, Location, IsAllDayEvent, ShowAs, IsRecurrence
                                 from Event where Id IN :eventIDs];
        syncEventsToActivities(eventList);
    }
    */
    /*
    * Sync Events to Activities when Events are upserted
    */
    public static void syncEventsToActivities(list<Event> eventList, boolean isUpdate){   
        
            if( eventList==null || eventList.isEMpty() ) return;
            //Get source activities (i.e Activities that are not referencing to other events)
            list<Event> sourceList = new list<Event>();
            for(Event a : eventList){
                if( !String.isBlank( a.Activity_ID__c) || !String.isBlank(a.Job_Allocation_ID__c)) continue;
                if(a.IsRecurrence) continue;//Do not sync Calendar Series
                sourceList.add( a );
            }
            if(sourceList.isEmpty()) return;
    
            list<id> lstWhoIDs = new list<id>();
            list<String> eventIDs   = new list<String> ();
    
            for(Event e: sourceList){
                lstWhoIDs.add(e.OwnerId);
                eventIDs.add( e.Id );
            }
        
            list<sked__Resource__c> lstRes = [select id,sked__Home_Address__c , sked__User__c from sked__Resource__c where sked__User__c in:lstWhoIDS];
            map<id,sked__Resource__c> map_User_Res = new map<id,sked__Resource__c>();
    
            for(sked__Resource__c res: lstRes){
                map_User_Res.put(res.sked__User__c,res);
            }
            //Search for related activities
            list<sked__Activity__c> relatedActivities = [Select Id, Event_ID__c from sked__Activity__c where Event_ID__c IN :eventIDs order by Name asc];
            Map<Id,Id> mapEventIdToActivityId = new Map<Id,Id>();
            for( sked__Activity__c a : relatedActivities ){
                mapEventIdToActivityId.put( Id.valueOf(a.Event_ID__c), a.Id );
            }
    
            list<sked__Activity__c> activitiesToUpsert = new list<sked__Activity__c>();
            Set<Id> activitiesToDelete = new Set<Id>();
    
            for(Event e: sourceList){
                Id aId = mapEventIdToActivityId.containsKey( e.Id )? mapEventIdToActivityId.get( e.Id ) : null;
                if(isUpdate && aId==null) continue;
                if(e.ShowAs == 'Free'){
                    activitiesToDelete.add(aId);
                    continue;
                }else if(map_User_Res.containskey(e.OwnerId)){
                    activitiesToUpsert.add( syncEventToActivity(e, map_User_Res.get(e.OwnerId), aId) );
                }
            }
    
            if(activitiesToUpsert.size() > 0){
                Schema.SObjectField eventID = sked__Activity__c.Event_ID__c.getDescribe().getSObjectField();
                Database.upsert(activitiesToUpsert, eventID, false)  ;
            }
            if(!activitiesToDelete.isEmpty()) Database.delete([select Id from sked__Activity__c where Id IN :activitiesToDelete]);
    }
    
    /*
    * Sync an Event to an Activity
    */
    public static sked__Activity__c syncEventToActivity(Event e, sked__Resource__c resource, Id activityId){
        System.debug(e.StartDateTime);
        System.debug(e.EndDateTime);
        sked__Activity__c a = new sked__Activity__c(
            sked__Type__C       = e.Subject,
            //sked__Notes__c      = e.Description,
            sked__Start__c      = e.StartDateTime,
            sked__End__c        = e.EndDateTime,
            sked__Address__c    = e.Location, 
            Event_ID__c         = e.Id,
            All_Day_Activity__c = e.IsAllDayEvent,
            sked__Resource__c   = resource.id
        );
        if(e.IsAllDayEvent){
            DateTime dt = e.StartDateTime;
            a.sked__Start__c      = DateTime.newInstance(dt.year(), dt.month(), dt.day(), 0, 0, 0);
            a.sked__End__c        = DateTime.newInstance(dt.year(), dt.month(), dt.day(), 23, 59, 0);
            System.debug(a.sked__Start__c);
        }
        if(activityId != null){//Update an existing activity
            a.Id    = activityId;
        }
        return a;
    }

    /*
    * Return a list of ID of Dispatched or Confirmed Job Allocations
    */
    public static list<Id> getNewDispatchedJobAllocationIDs(list<sked__Job_Allocation__c> newList){
        list<Id> jaIDs = new list<Id>();

        for (sked__Job_Allocation__c so : newList) {
            if(so.sked__Status__c == 'Dispatched' || so.sked__Status__c=='Confirmed'){ // Dispatch
                jaIDs.add(so.id);
            }
        }

        return jaIDs;
    }

    /*
    * Return a list of ID of Job Allocations which status changed to one of status in statusSet
    */
    public static list<Id> getJobAllocationIDs_StatusChanged(Set<String> statusSet, list<sked__Job_Allocation__c> newList, list<sked__Job_Allocation__c> oldList, map<Id, sked__Job_Allocation__c> oldMap){
        list<Id> jaIDs = new list<Id>();

        for (sked__Job_Allocation__c newJA : newList) {
            sked__Job_Allocation__c oldJA = oldMap.get(newJA.Id);
            if( newJA.sked__Status__c != null && statusSet.contains( newJA.sked__Status__c ) && newJA.sked__Status__c != oldJA.sked__Status__c){ 
                jaIDs.add( newJA.Id );
            }
        }

        return jaIDs;
    }

    /*
    * Sync Job Allocations to Events
    */
    public static void syncJobAllocationsToEvents(list<Id> jaIDs){
        if( jaIDs==null || jaIDs.isEmpty() ) return;

        list<Event> eventsToUpsert = new list<Event>();

        map<Id, sked__Job_Allocation__c> mapJAIdToJA = new map<Id,sked__Job_Allocation__c>(
            [select id, Name,sked__Job__r.sked__Contact__c, sked__Job__r.sked__Contact__r.Phone,sked__Job__r.sked__Account__c,sked__Resource__r.sked__User__c, 
                sked__Job__r.sked__Address__c,sked__Job__r.sked__Account__r.Name, sked__Job__r.sked__Contact__r.Name, sked__Job__r.Name, sked__Job__r.sked__Start__c, 
                sked__Job__r.sked__Finish__c, sked__Job__r.sked__Notes_Comments__c , sked__Job__r.sked__Type__c, 
                sked__Job__r.sked__Location__r.Name
            from sked__Job_Allocation__c 
            where Id IN :jaIDs and sked__Job__r.sked__Start__c != null and sked__Job__r.sked__Finish__c != null and sked__Resource__r.sked__User__c!=null]
        );

        //Search for related Events
        list<Event> relatedEvents = [Select Id, Job_Allocation_ID__c from Event where Job_Allocation_ID__c IN :mapJAIdToJA.keySet()];
        Map<Id,Id> mapJAIdToEventId = new Map<Id,Id>();
        
        for( Event e : relatedEvents ){
            mapJAIdToEventId.put( Id.valueOf(e.Job_Allocation_ID__c), e.Id );
        }

        for (sked__Job_Allocation__c ja : mapJAIdToJA.values()) {
            Id eId = mapJAIdToEventId.containsKey( ja.Id )? mapJAIdToEventId.get( ja.Id ) : null;
            eventsToUpsert.add( syncJobAllocationToEvent(ja, eId) );         
        }
        if(eventsToUpsert.size() > 0){
            upsert eventsToUpsert;
        }        
    }

    /*
    * Sync a Job Allocation to an Event record
    */
    public static Event syncJobAllocationToEvent(sked__Job_Allocation__c ja, Id eventId){                
        Event e = new Event(
            OwnerId         = ja.sked__Resource__r.sked__User__c,
            Location        = ja.sked__Job__r.sked__Address__c,
            StartDateTime   = ja.sked__Job__r.sked__Start__c,
            EndDateTime     = ja.sked__Job__r.sked__Finish__c,
            Job_Allocation_ID__c    = ja.Id,
			Subject         = fs(ja.sked__Job__r.sked__Account__r.Name, null)       
        );
        
        if(ja.sked__job__r.sked__Contact__c != null){
            e.WhoID     = ja.sked__Job__r.sked__Contact__c;
            //e.Location  += ' ' + fs(ja.sked__Job__r.sked__Contact__r.Phone, null);
        }
        if(eventId != null){//Update an existing event
            e.Id    = eventId;
        }
        return e;
    }
    
    /*
    * Delete related events when an event series is deleted
    */
    public static void deleteEventSeries(list<Event> events){
        Set<Id> eventSeriesID = new Set<Id>();
        for(Event e : events){
            if(e.IsRecurrence == true && e.Id == e.RecurrenceActivityId){
                eventSeriesID.add( e.Id );
            }
        }
        if(!eventSeriesID.isEmpty()) Database.delete([Select Id from Event where RecurrenceActivityId IN :eventSeriesID and (NOT ID IN :events)]);
    }

    /*
    * Format a String, return defaultValue if null
    */
    public static String fs(String s, String defaultValue){
        if( !String.isBlank(s) ) return s;
        else if( !String.isBlank( defaultValue ) ) return defaultValue;
        return '';
    }

    /*
    * Delete all Events related to Job Allocations
    */
    public static void deleteRelatedEvents(list<sked__Job_Allocation__c> jaList){
        if( jaList==null || jaList.isEmpty() ) return;

        list<id> allIds = new list<id>();
        for (sked__Job_Allocation__c so : jaList) {
            allIds.add(so.id);
        }

        list<Event> lstEvent = [select id from Event where Job_Allocation_ID__c in:allIds];

        if(lstEvent.size() > 0){
            delete lstEvent;
        }
    }

    /*
    * Return a list of ID of Job Allocations which status changed to one of status in statusSet
    */
    public static list<Id> getJobIDs_StartEndTimeChanged(list<sked__Job__c> newList, list<sked__Job__c> oldList, map<Id, sked__Job__c> oldMap){
        list<Id> jIDs = new list<Id>();

        for (sked__Job__c newRec : newList) {
            sked__Job__c oldRec = oldMap.get(newRec.Id);
            if(newRec.sked__Start__c != null && newRec.sked__Finish__c != null && (newRec.sked__Start__c != oldRec.sked__Start__c || newRec.sked__Finish__c != oldRec.sked__Finish__c)){
                jIDs.add( newRec.Id );
            }
        }

        return jIDs;
    }

    /*
    * Return a list of ID of Jobs which status changed to one of status in statusSet
    */
    public static list<Id> getJobIDs_StatusChanged(Set<String> statusSet, list<sked__Job__c> newList, list<sked__Job__c> oldList, map<Id, sked__Job__c> oldMap){
        list<Id> jaIDs = new list<Id>();

        for (sked__Job__c newRec : newList) {
            sked__Job__c oldRec = oldMap.get(newRec.Id);
            if( newRec.sked__Job_Status__c != null && statusSet.contains( newRec.sked__Job_Status__c ) && newRec.sked__Job_Status__c != oldRec.sked__Job_Status__c){ 
                jaIDs.add( newRec.Id );
            }
        }

        return jaIDs;
    }

    /*
    * Delete Activities related to deleted events
    */
    public static void deleteActivities(list<Event> events){
        //skedLogger.log(null, JSON.serialize(events), 'skedTriggerUtil', 'deleteActivities', skedLogger.DELETE_EVENT,'');
        Set<Id> eventIDs = new Set<Id>();
        Set<String> activityIDs = new Set<String>();
        for( Event e : events ){
            eventIDs.add( e.Id );
            if(!String.isBlank(e.Activity_ID__c)) activityIDs.add(e.Activity_ID__c);
        }
        list<sked__Activity__c> aList = [Select Id from sked__Activity__c where Event_Id__c IN :eventIDs or Id IN :activityIDs];
        if( !aList.isEmpty() ) Database.delete( aList );
    }
}