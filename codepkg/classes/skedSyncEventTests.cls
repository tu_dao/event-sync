@isTest
private class skedSyncEventTests {
	
  @testSetup
  static void setupCommonTestData(){
        //map<String,list<sObject>> customSettingMap = skedTestDataFactory.createCustomSettingData();

        list<Account> accounts = skedTestDataFactory.createAccounts('Test Account', 'Organization', 1);
        insert accounts;

        sked__Region__c sydney = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert sydney;

        Contact client = skedTestDataFactory.createContact(accounts.get(0).Id, 'Test Client', 'Customer', sydney.Id);
        insert client;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney',null);
        insert region;

        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', accounts.get(0).Id, sydney.id);
        insert location;

        sked__Resource__c resource = skedTestDataFactory.createResource('Admin', UserInfo.getUserId(), sydney.Id);
        insert resource;
       
    }

	//Test to create Event
	@isTest 
  static void CreateEvent() {
   		Event e = skedTestDataFactory.createEvent(UserInfo.getUserId());
		  insert e;
		  delete e;
	}

	//Test to create Activity
	@isTest static void CreateActivity() {
      sked__Resource__c resource = [Select Id from sked__Resource__c limit 1];
   		sked__Activity__c act = skedTestDataFactory.createActivity(resource.Id, null);
		  insert act;

      delete act;
	}

	//Test to create Activity
	@isTest static void CreateJobAndDispatch() {
    Account account = [Select Id from Account limit 1];
    Contact contact = [Select Id from Contact limit 1];
    sked__Region__c region = [Select Id from sked__Region__c limit 1];
    sked__Resource__c resource = [Select Id from sked__Resource__c limit 1];

    sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 1).get(0);
    job.sked__Account__c = account.id;
    job.sked__Contact__c = contact.Id;
    job.sked__Job_Status__c = 'Dispatched';
    insert job;

    sked__Job_Allocation__c ja = skedTestDataFactory.allocateJob(job.Id, resource.Id);
		ja.sked__Status__c = 'Dispatched';
		insert ja;

    //Update Job Start Time
    job.sked__Start__c  = job.sked__Start__c.addHours(1);
    job.sked__Finish__c = job.sked__Finish__c.addHours(1);
    update job;

    ja.sked__Status__c = 'Deleted';
    update ja;

    //delete jobs
    delete job;
	}

	//Test to create Activity
	@isTest static void DeleteJobAllocation() {
    Account account = [Select Id from Account limit 1];
    Contact contact = [Select Id from Contact limit 1];
    sked__Region__c region = [Select Id from sked__Region__c limit 1];
    sked__Resource__c resource = [Select Id from sked__Resource__c limit 1];

    sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 1).get(0);
    job.sked__Account__c = account.id;
    job.sked__Contact__c = contact.Id;
    job.sked__Job_Status__c = 'Dispatched';
    insert job;

    sked__Job_Allocation__c ja = skedTestDataFactory.allocateJob(job.Id, resource.Id);
    ja.sked__Status__c = 'Dispatched';
    insert ja;

    delete ja;
	}
}