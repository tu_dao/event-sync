# README #

### What is this repository for? ###

* This component enables synchronization between Skedulo Jobs and Activities, and Salesforce events (calendars). 
* Version 0.1

### How do I get set up? ###

* Download the deployment package
* Update credentials of target organization in build.properties
* Deploy (or validate) the package using [Force.com Migration Tool](https://developer.salesforce.com/page/Force.com_Migration_Tool)
* Update profiles/permission sets to grant access to custom fields of Salesforce Activity (Activity ID, Job Allocation ID) and Skedulo Activity (Event ID, All Day Activity)
```
#!bash

ant deployCode -verbose
ant validateCode -verbose 
```


### Who do I talk to? ###

* Tu Dao (tdao@skedulo.com)